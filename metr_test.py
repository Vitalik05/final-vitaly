from metr import main


def test_main(monkeypatch):
    monkeypatch.setattr("sys.argv", ['python3', "150", "millimeters", "decimeter"])
    assert main() == "1.5 ,"
    monkeypatch.setattr("sys.argv", ['python3', "0", "millimeters", "kilometer"])
    assert main() == "0.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "70", "millimeters", "centimeter"])
    assert main() == "7.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "100", "millimeters", "metre"])
    assert main() == "0.1 ,"
    monkeypatch.setattr("sys.argv", ['python3', "30", "centimeter", "millimeters"])
    assert main() == "300.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "100", "centimeter", "metre"])
    assert main() == "1.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "300", "centimeter", "kilometer"])
    assert main() == "0.003 ,"
    monkeypatch.setattr("sys.argv", ['python3', "40", "centimeter", "decimeter"])
    assert main() == "4.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "30", "decimeter", "metre"])
    assert main() == "3.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "0.6", "decimeter", "centimeter"])
    assert main() == "6.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "20", "decimeter", "kilometer"])
    assert main() == "0.002 ,"
    monkeypatch.setattr("sys.argv", ['python3', "0.1", "decimeter", "millimeters"])
    assert main() == "10.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "1000", "millimeters", "metre"])
    assert main() == "1.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "10000", "millimeters", "kilometer"])
    assert main() == "0.01 ,"
    monkeypatch.setattr("sys.argv", ['python3', "50", "millimeters", "centimeter"])
    assert main() == "5.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "7", "millimeters", "decimeter"])
    assert main() == "0.07 ,"
    monkeypatch.setattr("sys.argv", ['python3', "5", "kilometer", "metre"])
    assert main() ==  "5000.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "0.5", "kilometer", "millimeters"])
    assert main() == "500000.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "1", "kilometer", "decimeter"])
    assert main() == "10000.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "3", "kilometer", "centimeter"])
    assert main() == "300000.0 ,"
    monkeypatch.setattr("sys.argv", ['python3', "tgrt", "kilometer", "centimeter"])
    assert main() == "пишите цифрами"
    monkeypatch.setattr("sys.argv", ['python3', "3", "frgr", "centimeter"])
    assert main() == "введите данные коректно"
