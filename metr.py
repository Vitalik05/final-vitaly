import sys

def transporate(*args):
    if (len(args) == 3):
        if (args[1] == "millimeters"):
            if(args[2] == "centimeter"):
                return args[0] / 10
            elif(args[2] == "decimeter"):
                return args[0] / 100
            elif(args[2] == "metre"):
                return args[0] / 1000
            elif(args[2] == "kilometer"):
                return args[0] / 1000000
        
        if (args[1] == "centimeter"):
            if(args[2] == "millimeters"):
                return args[0] * 10
            elif(args[2] == "decimeter"):
                return args[0] / 10
            elif(args[2] == "metre"):
                return args[0] / 100
            elif(args[2] == "kilometer"):
                return args[0] / 100000

        if (args[1] == "decimeter"):
            if(args[2] == "millimeters"):
                return args[0] * 100
            elif(args[2] == "centimeter"):
                return args[0] * 10
            elif(args[2] == "metre"):
                return args[0] / 10
            elif(args[2] == "kilometer"):
                return args[0] / 10000

        if (args[1] == "metre"):
            if(args[2] == "millimeters"):
                return args[0] * 1000
            elif(args[2] == "centimeter"):
                return args[0] * 100
            elif(args[2] == "decimeter"):
                return args[0] / 10
            elif(args[2] == "kilometer"):
                return args[0] / 1000

        if (args[1] == "kilometer"):
            if(args[2] == "millimeters"):
                return args[0] * 1000000
            elif(args[2] == "centimeter"):
                return args[0] * 100000
            elif(args[2] == "decimeter"):
                return args[0] * 10000
            elif(args[2] == "metre"):
                return args[0] * 1000


def main():
    if len(sys.argv) > 2:
        try:
            z=f"{transporate(float(sys.argv[1]), sys.argv[2], sys.argv[3])} ,"
            if z=="None ,":
                return "введите данные коректно"
            print(z)
            return z
        except Exception:
            print("пишите цифрами")
            return "пишите цифрами"
    else:
        print("введите данные коректно")
        return "введите данные коректно"

if __name__ == '__main__':
    main()